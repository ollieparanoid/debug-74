#!/bin/sh -ex
# Run in second terminal after run.sh was started, to attach gdb
alias pmbootstrap=./pmbootstrap/pmbootstrap.py
pmbootstrap -q chroot -- \
	gdb-multiarch -q \
		-ex 'set architecture armv7' \
		-ex 'set debug-file-directory /armv7/usr/lib/debug/' \
		-ex 'file /armv7/usr/lib/qt5/bin/qmlplugindump' \
		-ex 'set sysroot /armv7' \
		-ex 'target remote localhost:1337'

#		-ex 'continue' \
#		-ex 'bt'
