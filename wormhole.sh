#!/bin/sh -e
sudo apk -q add \
	py3-attrs \
	py3-automat \
	py3-click \
	py3-pip \
	py3-pynacl \
	py3-six \
	py3-tqdm \
	py3-twisted
sudo pip3 -q install magic-wormhole
wormhole "$@"
