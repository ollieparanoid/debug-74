#!/bin/sh -ex
alias pmbootstrap="pmbootstrap/pmbootstrap.py"
WORK=~/.local/var/pmbootstrap

sudo apk -q add neovim htop

# install in native chroot
# pmbootstrap -q chroot -- apk add -q \
# 	gdb-multiarch

# install in armv7 chroot
# pmbootstrap -q chroot -barmv7 -- apk add -q \
# 	qt5-qtdeclarative-dbg \
# 	musl-dbg

# Mount armv7 chroot in native chroot
# sudo mkdir -p "$WORK/chroot_native/armv7"
# sudo mount --bind "$WORK/chroot_buildroot_armv7" "$WORK/chroot_native/armv7"


# Start hanging command and wait for gdb
# echo "(waiting for gdb)"
# pmbootstrap -q chroot -barmv7 -- \
# 	/usr/bin/qemu-arm-static -g 1337 \
#		/usr/lib/qt5/bin/qmlplugindump org.kde.kirigamiaddons.dateandtime 0.1

# use QEMU strace
pmbootstrap -q chroot -barmv7 -- \
	/usr/bin/qemu-arm-static -strace \
		/usr/lib/qt5/bin/qmlplugindump org.kde.kirigamiaddons.dateandtime 0.1 2>&1 | tee out.txt

debug-74/wormhole.sh out.txt
